package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button btnwork;
    public ProgressBar pbwork;
    public ImageView iv;
    public void onWorking(ActionEvent actionEvent) {
        Task task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                final int max = 100;
                pbwork.setVisible(true);
                for (int i = 1; i <= max; i++) {
                    updateProgress(i,max);
                    Thread.sleep(100);
                }
                return null;
            }
            @Override
            protected void scheduled() {
                super.scheduled();
            }
            @Override
            protected void succeeded() {
                super.succeeded();
                pbwork.setVisible(false);
            }
            @Override
            protected void failed(){
                super.failed();
            }
        };
        pbwork.progressProperty().bind(task.progressProperty());

    new Thread(task).start();
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        for (int i =1 ;i<6;i++){

            int j = i ;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource("/png/".concat(String.valueOf(j)).concat(".png")).toString()));
                        }
                    })
            );
        }
        timeline.play();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {




        
    }

}
